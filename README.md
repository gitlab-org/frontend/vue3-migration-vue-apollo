# 🚀 VueApollo migration demo

This project is intended to demonstrate migration of VueApollo when using `@vue/compat`

`main` branch contains initial state of migration, make sure to browse other branches to see step-by-step migration

You can run Vue.js 2 version with

```
yarn serve
```

or Vue.js 3 version (@vue/compat) with

```
yarn serve:vue3
```
