const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const USE_VUE3 = "USE_VUE3" in process.env;

let VUE3_ALIASES = {};
if (USE_VUE3) {
  console.log("[!!!] Using Vue.js 3");
  const moduleAlias = require("module-alias");
  moduleAlias.addAlias("vue/compiler-sfc", "@vue/compiler-sfc");

  VUE3_ALIASES = {
    vue: "@vue/compat",
  };
}

const VUE_LOADER = USE_VUE3 ? "vue-loader-vue3" : "vue-loader";
const { VueLoaderPlugin } = require(VUE_LOADER);

const isProduction = process.env.NODE_ENV == "production";

const stylesHandler = "style-loader";

const config = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
  },
  resolve: { alias: USE_VUE3 ? VUE3_ALIASES : {} },
  devServer: {
    open: true,
    host: "localhost",
  },
  plugins: [
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: true,
      __VUE_PROD_DEVTOOLS__: true,
    }),
    new HtmlWebpackPlugin({
      template: "index.html",
    }),
    new VueLoaderPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/i,
        loader: "babel-loader",
      },
      {
        test: /\.css$/i,
        use: [stylesHandler, "css-loader"],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
        type: "asset",
      },
      {
        test: /\.vue$/,
        loader: VUE_LOADER,
      },
    ],
  },
  devtool: "eval-cheap-module-source-map",
};

module.exports = () => {
  if (isProduction) {
    config.mode = "production";
  } else {
    config.mode = "development";
  }
  return config;
};
