import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
} from "@apollo/client/core";

const httpLink = createHttpLink({
  uri: "https://api.spacex.land/graphql/",
});

export default () => {
  return new ApolloClient({
    link: httpLink,
    connectToDevTools: process.env.NODE_ENV !== "production",
    cache: new InMemoryCache(),
  });
};
