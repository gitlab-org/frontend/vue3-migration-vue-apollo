import Vue from "vue";
import VueApollo from "vue-apollo";

import Demo from "./components/Demo.vue";
import createDefaultClient from "./lib/graphql";

Vue.use(VueApollo);

const apolloProvider = new VueApollo({
  defaultClient: createDefaultClient(),
});

new Vue({
  el: "#app",
  apolloProvider,
  render(h) {
    return h(Demo);
  },
});
